import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class TwitterDummyGenerator {
	private static Random rand = new Random(); 
    private static String []name = {
    	"Ace", "Adi", "Andi", "Anton", "Apollo", "Arya", "Ava", "Axel", 
    	"Bayu", "Bella", "Bima", "Blake", "Briar", "Brooke", "Budi", "Cade", 
    	"Charlie", "Chase", "Cita", "Citra", "Cleo", "Dahlia", "Daisy", 
    	"Dante", "Deni", "Dian", "Dora", "Elara", "Eli", "Elio", "Endang", 
    	"Ethan", "Fajar", "Felix", "Fiona", "Fiona", "Fitri", "Freya", "Gemma", 
    	"George", "Gia", "Gilang", "Gina", "Grace", "Hadi", "Harper", "Hazel", 
    	"Hendra", "Henry", "Hugo", "Ika", "Imani", "Inara", "Indra", "Isaac", 
    	"Ivy", "Jace", "Jack", "Jasmine", "Jaxon", "Jaya", "Joko", "Kaya", 
    	"Keira", "Kevin", "Kiki", "Kira", "Kusuma", "Leo", "Liam", "Lily", 
    	"Lina", "Luca", "Luna", "Lusi", "Mason", "Max", "Mia", "Miko", "Mila", 
    	"Mira", "Nia", "Nia", "Nico", "Nita", "Noah", "Nora", "Oka", "Oki", 
    	"Olive", "Oliver", "Ophelia", "Owen", "Penny", "Phoenix", "Piper", 
    	"Poppy", "Puri", "Putra", "Quinlan", "Quinn", "Quinn", "Rex", "Rika", 
    	"Rina", "Ruby", "Ryder", "Ryder", "Sadie", "Sage", "Santi", "Sari", 
    	"Silas", "Stella", "Thalia", "Theo", "Theo", "Tika", "Titan", "Tono", 
    	"Ulysses", "Uma", "Umar", "Usman", "Vada", "Vera", "Viki", "Vina", 
    	"Violet", "Wahyu", "Wesley", "Widya", "Wren", "Wyatt", "Wyatt", "Xander", 
    	"Xena", "Xena", "Xiomara", "Yani", "Yara", "Yara", "Yazmin", "Yogi", 
    	"Zain", "Zane", "Zara", "Zara", "Zephyr", "Zoey"};
	private static String []acc_types = {"regular", "premium", "admin"};
	private static String []genders = {"man", "woman"};
	private static ArrayList<String> allCreatedNames = new ArrayList<>();
    private static final Map<String, List<String>> countryCitiesMap = new HashMap<>();
    static {
        countryCitiesMap.put("Indonesia", Arrays.asList("Jakarta", "Bandung", "Surabaya", "Yogyakarta", "Medan", "Semarang", "Makassar", "Palembang", "Balikpapan", "Denpasar"));
        countryCitiesMap.put("United States", Arrays.asList("New York", "Los Angeles", "Chicago", "Houston", "San Francisco", "Miami", "Las Vegas", "Seattle", "Boston", "Dallas"));
        countryCitiesMap.put("United Kingdom", Arrays.asList("London", "Manchester", "Birmingham", "Edinburgh", "Liverpool", "Glasgow", "Bristol", "Oxford", "Cambridge", "Leeds"));
        countryCitiesMap.put("Australia", Arrays.asList("Sydney", "Melbourne", "Brisbane", "Perth", "Adelaide", "Gold Coast", "Canberra", "Newcastle", "Hobart", "Darwin"));
        countryCitiesMap.put("Germany", Arrays.asList("Berlin", "Munich", "Hamburg", "Frankfurt", "Cologne", "Stuttgart", "Dresden", "Dusseldorf", "Nuremberg", "Hannover"));
        countryCitiesMap.put("France", Arrays.asList("Paris", "Marseille", "Lyon", "Toulouse", "Nice", "Nantes", "Strasbourg", "Montpellier", "Bordeaux", "Lille"));
        countryCitiesMap.put("Canada", Arrays.asList("Toronto", "Vancouver", "Montreal", "Calgary", "Ottawa", "Edmonton", "Quebec City", "Winnipeg", "Hamilton", "London"));
        countryCitiesMap.put("China", Arrays.asList("Beijing", "Shanghai", "Guangzhou", "Shenzhen", "Chengdu", "Hangzhou", "Chongqing", "Wuhan", "Tianjin", "Xian"));
        countryCitiesMap.put("Japan", Arrays.asList("Tokyo", "Osaka", "Kyoto", "Yokohama", "Nagoya", "Sapporo", "Fukuoka", "Kobe", "Hiroshima", "Nara"));
        countryCitiesMap.put("India", Arrays.asList("Mumbai", "Delhi", "Bangalore", "Chennai", "Kolkata", "Hyderabad", "Pune", "Ahmedabad", "Jaipur", "Surat"));
        countryCitiesMap.put("South Korea", Arrays.asList("Seoul", "Busan", "Incheon", "Daegu", "Daejeon", "Gwangju", "Suwon", "Ulsan", "Sejong", "Jeju"));
        countryCitiesMap.put("Brazil", Arrays.asList("Sao Paulo", "Rio de Janeiro", "Salvador", "Brasilia", "Fortaleza", "Belo Horizonte", "Manaus", "Curitiba", "Recife", "Porto Alegre"));
        countryCitiesMap.put("Russia", Arrays.asList("Moscow", "Saint Petersburg", "Novosibirsk", "Yekaterinburg", "Nizhny Novgorod", "Kazan", "Chelyabinsk", "Omsk", "Samara", "Rostov-on-Don"));
        countryCitiesMap.put("Italy", Arrays.asList("Rome", "Milan", "Naples", "Turin", "Florence", "Venice", "Bologna", "Verona", "Genoa", "Palermo"));
        countryCitiesMap.put("Spain", Arrays.asList("Madrid", "Barcelona", "Valencia", "Seville", "Bilbao", "Malaga", "Palma de Mallorca", "Granada", "Zaragoza", "Alicante"));
        countryCitiesMap.put("Mexico", Arrays.asList("Mexico City", "Guadalajara", "Monterrey", "Puebla", "Tijuana", "Queretaro", "Cancun", "Merida", "Acapulco", "Oaxaca"));
        countryCitiesMap.put("Netherlands", Arrays.asList("Amsterdam", "Rotterdam", "The Hague", "Utrecht", "Eindhoven", "Groningen", "Maastricht", "Haarlem", "Delft", "Leiden"));
        countryCitiesMap.put("Argentina", Arrays.asList("Buenos Aires", "Cordoba", "Rosario", "Mendoza", "La Plata", "Mar del Plata", "Salta", "San Juan", "Santa Fe", "San Miguel de Tucuman"));
        countryCitiesMap.put("Turkey", Arrays.asList("Istanbul", "Ankara", "Izmir", "Bursa", "Antalya", "Adana", "Gaziantep", "Konya", "Mersin", "Diyarbakir"));
        countryCitiesMap.put("Egypt", Arrays.asList("Cairo", "Alexandria", "Giza", "Sharm El Sheikh", "Luxor", "Aswan", "Hurghada", "Port Said", "Mansoura", "Suez"));
    }
    private static final String[] WORDS = {
	    "Hari", "ini", "saya", "merasa", "bahagia",
	    "Kucing", "saya", "lucu", "sekali",
	    "Pemandangan", "di", "sini", "indah", "banget",
	    "Sudah", "waktunya", "liburan", "lagi",
	    "Bosan", "banget", "di", "rumah",
	    "Makanan", "favorit", "saya", "adalah", "nasi",
	    "Saya", "sedang", "membaca", "buku", "baru",
	    "Mendung", "hari", "ini", "serasa", "romantis",
	    "Musik", "adalah", "penghibur", "terbaik",
	    "Senyum", "hari", "ini", "bikin", "semangat",
	    "Bebek", "goreng", "sambal", "ijo", "mantap",
	    "Cinta", "itu", "indah", "dan", "membahagiakan"
	};
	public static void main(String[] args) {
		try (BufferedWriter writer1 = new BufferedWriter(new FileWriter("dummyUsers.sql"))) {
			for (int i=1; i<=1500; i++) {
		        int id = i;
		        String nickname = generateNickname();
		        allCreatedNames.add(nickname);
		        String username = generateUsername(nickname);
		        String email = generateEmail(nickname);
		        String password = generatePassword(nickname);
		        String acc_type = generateAccType();
		        writer1.write("INSERT INTO \"Users\"(id, username, nickname, email, password, acc_type) " + 
		        	"VALUES (" + id + ",\'" + username + "\',\'" + nickname + "\',\'" + email + "\',\'" + password + "\',\'" + acc_type + "\')" + ((i<1500)? ";":""));
				if (i < 1500) {
				    writer1.newLine();
				}
		    }
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    try (BufferedWriter writer2 = new BufferedWriter(new FileWriter("dummyUsers_detail.sql"))) {
			for (int i=1; i<=1500; i++) {
		        int id = i;
				String [] fullname = allCreatedNames.get(i-1).split(" ");
				String first_name = fullname[0];
				String last_name = fullname[1];
				String country = generateCountry();
				String city = generateCity(country);
				String birthdate = generateDate(1970, 2005);
				String join_date = generateDate(2019, 2022);
				String gender = generateGender();
		        writer2.write("INSERT INTO \"Users_detail\"(id, first_name, last_name, country, city, birthdate, join_date, gender) " + 
	        		"VALUES (" + id + ",\'" + first_name + "\',\'" + last_name + "\',\'" + country + "\',\'" + city + "\',\'" + birthdate + "\',\'" + join_date + "\',\'" + gender + "\')" + ((i<1500)? ";":""));
				if (i < 1500) {
				    writer2.newLine();
				}
		    }
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		try (BufferedWriter writer3 = new BufferedWriter(new FileWriter("dummyFollows.sql"))) {
			for (int i=1; i<=100000; i++) {
				int id = i;
				int following_id = rand.nextInt(1500) + 1;
				int followed_id = rand.nextInt(20) + 1;
			    writer3.write("INSERT INTO \"Follows\"(id, following_id, followed_id) " + "VALUES (" + id + "," + following_id + "," + followed_id + ")" + ((i<100000)? ";":""));
				if (i < 100000) {
				    writer3.newLine();
				}
			}
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	    try (BufferedWriter writer4 = new BufferedWriter(new FileWriter("dummyTweets.sql"))) {
			for (int i=1; i<=100000; i++) {
				int id = i;
				int user_id = rand.nextInt(1500) + 1;
				String tweet_text = generateTweet();
				String date_created = generateDate(2023, 2023);
		        String time_created = generateTime();
			    writer4.write("INSERT INTO \"Tweets\"(id, user_id, tweet_text, date_created, time_created) " + 
	        		"VALUES (" + id + "," + user_id + ",\'" + tweet_text + "\',\'" + date_created + "\',\'" + time_created + "\')" + ((i<100000)? ";":""));
				if (i < 100000) {
				    writer4.newLine();
				}
			}
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		try (BufferedWriter writer5 = new BufferedWriter(new FileWriter("dummyLikes.sql"))) {
			for (int i=1; i<=40000; i++) {
				int id = i;
				int user_id = rand.nextInt(1500) + 1;
				int tweet_id = rand.nextInt(20) + 1;
			    writer5.write("INSERT INTO \"Likes\"(id, user_id, tweet_id) " + "VALUES (" + id + "," + user_id + "," + tweet_id + ")" + ((i<40000)? ";":""));
				if (i < 40000) {
				    writer5.newLine();
				}
			}
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		try (BufferedWriter writer6 = new BufferedWriter(new FileWriter("dummyRetweets.sql"))) {
			for (int i=1; i<=40000; i++) {
				int id = i;
				int tweet_id = rand.nextInt(1500) + 1;
				int retweet_to_tweet_id = rand.nextInt(20) + 1;
			    writer6.write("INSERT INTO \"Retweets\"(id, tweet_id, retweet_to_tweet_id) " + "VALUES (" + id + "," + tweet_id + "," + retweet_to_tweet_id + ")" + ((i<40000)? ";":""));
				if (i < 40000) {
				    writer6.newLine();
				}
			}
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
		try (BufferedWriter writer7 = new BufferedWriter(new FileWriter("dummyReplies.sql"))) {
			for (int i=1; i<=40000; i++) {
				int id = i;
				int tweet_id = rand.nextInt(1500) + 1;
				int reply_to_tweet_id = rand.nextInt(20) + 1;
			    writer7.write("INSERT INTO \"Replies\"(id, tweet_id, reply_to_tweet_id) " + "VALUES (" + id + "," + tweet_id + "," + reply_to_tweet_id + ")" + ((i<40000)? ";":""));
				if (i < 40000) {
				    writer7.newLine();
				}
			}
	    } catch (IOException e) {
	        e.printStackTrace();
	    }
	}
	public static String generateNickname() {
		String first_name = name[rand.nextInt(150)];
		String last_name = name[rand.nextInt(150)];
		return first_name + " " + last_name;
	}
	public static String generateUsername(String fullname) {
		String [] arr = fullname.split(" ");
		return arr[0].toLowerCase() + arr[1].toLowerCase() + rand.nextInt(1000);
	}
	public static String generateEmail(String fullname) {
		String [] arr = fullname.split(" ");
		return arr[0].toLowerCase() + arr[1].toLowerCase() + rand.nextInt(1000) + "@gmail.com";
	}
	public static String generatePassword(String fullname) {
		int leftLimit = 48; // numeral '0'
	    int rightLimit = 122; // letter 'z'
	    int targetStringLength = fullname.length() - 1;

	    String generatedString = rand.ints(leftLimit, rightLimit + 1)
	      .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
	      .limit(targetStringLength)
	      .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
	      .toString();

		return generatedString;
	}
	public static String generateAccType() {
		return acc_types[rand.nextInt(3)];
	}
	public static String generateCountry() {
        List<String> countries = new ArrayList<>(countryCitiesMap.keySet());
        Random random = new Random();
        int index = random.nextInt(countries.size());
        return countries.get(index);
    }
    public static String generateCity(String country) {
        List<String> cities = countryCitiesMap.get(country);
        if (cities != null && !cities.isEmpty()) {
            Random random = new Random();
            int index = random.nextInt(cities.size());
            return cities.get(index);
        }
        return "";
    }
	public static String generateDate(int startYear, int endYear) {
        Random random = new Random();
        int year = random.nextInt(endYear - startYear + 1) + startYear;
        int month = random.nextInt(12) + 1;
        int day = random.nextInt(getMaxDayOfMonth(year, month)) + 1;

        Calendar date = new GregorianCalendar(year, month - 1, day);
        day = date.get(Calendar.DAY_OF_MONTH);
        month = date.get(Calendar.MONTH) + 1;
        year = date.get(Calendar.YEAR);

        return String.format("%04d-%02d-%02d", year, month, day);
    }
    public static int getMaxDayOfMonth(int year, int month) {
        Calendar calendar = new GregorianCalendar(year, month - 1, 1);
        return calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
    }
    public static String generateGender() {
    	return genders[rand.nextInt(2)];
    }
    public static String generateTweet() {
        StringBuilder tweet = new StringBuilder();

        int tweetLength = rand.nextInt(6) + 5; // Panjang tweet antara 5 hingga 10 kata
        for (int i = 0; i < tweetLength; i++) {
            int randomIndex = rand.nextInt(WORDS.length);
            tweet.append(WORDS[randomIndex]).append(" ");
        }

        tweet.deleteCharAt(tweet.length() - 1);
        return tweet.toString();
    }
	public static String generateTime() {
        int h = rand.nextInt(24);
        int m = rand.nextInt(60);
        int s = rand.nextInt(60);
        return String.format("%02d:%02d:%02d", h, m, s);
    }
}
